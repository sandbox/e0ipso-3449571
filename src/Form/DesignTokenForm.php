<?php

declare(strict_types=1);

namespace Drupal\design_tokens\Form;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\design_tokens\Entity\DesignToken;

/**
 * Design Token form.
 */
final class DesignTokenForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [DesignToken::class, 'load'],
        'source' => ['name'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
    ];

    $options = array_map(
      static fn(string $type) => ucwords(preg_replace('/(?!^)[A-Z]{2,}(?=[A-Z][a-z])|[A-Z][a-z]/', ' $0', $type)),
      array_combine(DesignToken::getAllValidTypes(), DesignToken::getAllValidTypes())
    );

    $is_non_primitive = ($this->entity->get('layer') ?? DesignToken::LAYER_SEMANTIC) !== DesignToken::LAYER_PRIMITIVE;
    $form['is_non_primitive'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reference another token'),
      '#description' => $this->t('Check this if this token is aliased to another token. Uncheck this to provide a primitive value.'),
      '#default_value' => $is_non_primitive,
    ];
    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Token Type'),
      '#description' => $this->t('A token\'s type is a predefined categorization applied to the token\'s value. Token tools can use Types to infer the purpose of a token. For example: A translation tool might reference a token\'s type to convert the source value into the correct platform-specific format.'),
      '#options' => $options,
      '#default_value' => $this->entity->get('type'),
      '#states' => [
        'visible' => [':input[name=is_non_primitive]' => ['checked' => FALSE]],
        'required' => [':input[name=is_non_primitive]' => ['checked' => FALSE]],
      ],
    ];
    $form['non_primitive']['#tree'] = TRUE;
    $form['non_primitive']['value'] = [
      '#type' => 'select',
      '#title' => $this->t('Token alias'),
      '#description' => $this->t('The ID of another token'),
      '#default_value' => $this->entity->get('value') ?? '',
      '#options' => array_map(
        static fn (DesignToken $token) => '[' . ucwords($token->get('layer')) . '] ' . $token->label(),
        array_filter(
          $this->entityTypeManager->getStorage('design_token')->loadMultiple() ?? [],
          static fn (DesignToken $token) => $token->get('layer') !== DesignToken::LAYER_COMPONENT,
        ),
      ),
      '#states' => [
        'visible' => [':input[name=is_non_primitive]' => ['checked' => TRUE]],
        'required' => [':input[name=is_non_primitive]' => ['checked' => TRUE]],
      ],
    ];
    $form['primitive']['#tree'] = TRUE;
    $form['primitive']['value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Value'),
      '#description' => $this->t('The token value in YAML format.'),
      '#default_value' => Yaml::encode($this->entity->get('value')),
      '#states' => [
        'visible' => [':input[name=is_non_primitive]' => ['checked' => FALSE]],
        'required' => [':input[name=is_non_primitive]' => ['checked' => FALSE]],
      ],
    ];

    $form['needsSiteBuilderReview'] = [
      '#type' => 'hidden',
      '#value' => FALSE,
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    parent::copyFormValuesToEntity($entity, $form, $form_state);

    $parents = $form['#parents'] ?? [];
    // If the token is a reference, we consider it semantic. If not, we consider
    // it primitive. Component will also reference other tokens, but they are
    // not created using this form.
    $layer = $entity->get('layer');
    if ($layer !== DesignToken::LAYER_COMPONENT) {
      $layer = $form_state->getValue([...$parents, 'is_non_primitive'])
        ? DesignToken::LAYER_SEMANTIC
        : DesignToken::LAYER_PRIMITIVE;
      $entity->set('layer', $layer);
    }
    if ($layer === DesignToken::LAYER_PRIMITIVE) {
      // We don't want to store the JSON string in the entity. We store the raw
      // data, so we can use it with whatever format we want.
      try {
        $value = Yaml::decode($form_state->getValue([...$parents, 'primitive', 'value']));
        $entity->set('value', $value);
      }
      catch (InvalidDataTypeException $e) {
      }
    }
    else {
      $value = $form_state->getValue([...$parents, 'non_primitive', 'value']);
      $entity->set('value', $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $parents = $form['#parents'] ?? [];
    $array_parents = $form['#array_parents'] ?? [];
    $is_non_primitive = $form_state->getValue([...$parents, 'is_non_primitive']);
    if ($is_non_primitive) {
      $value = $form_state->getValue([...$parents, 'non_primitive', 'value']);
      // Validate that it references an actual token.
      $storage = $this->entityTypeManager->getStorage('design_token');
      if (!$storage->load($value)) {
        $form_state->setError(
          NestedArray::getValue($form, [...$array_parents, 'non_primitive', 'value']),
          $this->t('Invalid alias. This is not a valid design token ID: %id', ['%id' => $value])
        );
      }
    }
    else {
      $value = $form_state->getValue([...$parents, 'primitive', 'value']);
      // Validate that this is valid YAML.
      try {
        Yaml::decode($value);
      }
      catch (InvalidDataTypeException $e) {
        $form_state->setError(
          NestedArray::getValue($form, [...$array_parents, 'primitive', 'value']),
          $this->t('Invalid YAML code: %message', ['%message' => $e->getMessage()])
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match ($result) {
        \SAVED_NEW => $this->t('Created new example %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated example %label.', $message_args),
      }
    );
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
