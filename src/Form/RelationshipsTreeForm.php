<?php

namespace Drupal\design_tokens\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\design_tokens\Entity\DesignToken;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to set the relationships.
 *
 * A GoJS app is mounted on this form for UX reasons.
 */
class RelationshipsTreeForm extends FormBase {

  /**
   * Creates a new form object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'design_tokens_relationships_tree';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['diagram'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => ['id' => ['myDiagramDiv']],
    ];
    $form['data'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Data'),
      '#default_value' => $this->calculateDefaultValue(),
    ];
    $form['#attached']['library'][] = 'design_tokens/design_tokens.tree_diagram';
    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    return;
  }

  private function calculateDefaultValue(): string {
    $storage = $this->entityTypeManager->getStorage('design_token');
    $node_data = array_map(
      function (DesignToken $token) {
        $node_datum = [
          'key' => $token->id(),
          'name' => $token->label(),
          'title' => $token->get('layer'),
          'email' => $this->t('Edit this design token'),
          'phone' => $this->t('Delete this design token'),
        ];
        if ($token->isPrimitive()) {
          $node_datum['dept'] = $token->getValue();
        }
        else {
          $node_datum['parent'] = $token->getValue();
        }
        return $node_datum;
      },
      array_values($storage->loadMultiple()),
    );
    $data = [
      'class' => 'go.TreeModel',
      'nodeDataArray' => $node_data,
    ];
    return Json::encode($data);
  }

}
