<?php

declare(strict_types=1);

namespace Drupal\design_tokens\tom\Parser;

use Drupal\design_tokens\tom\DesignToken;
use Drupal\design_tokens\tom\Group;
use Drupal\design_tokens\tom\RootGroup;
use Drupal\design_tokens\tom\Serializer\CssVarsSerializer;

class DTCGParser
{
  public function parse(array $dtcg): RootGroup
  {
    $root = new RootGroup();
    $this->doParseDTCG($dtcg, '', $root);
    return $root;
  }

  protected function doParseDTCG(array $dtcg, string $prefix, Group $root): void
  {
    foreach ($dtcg as $name => $group_or_token) {
      if (is_array(($group_or_token))) {
        $new_token_name = ($prefix ? $prefix . CssVarsSerializer::GROUP_SEPARATOR : '') . $name;
        if (array_key_exists('$value', $group_or_token)) {
          // @todo: Handle nesting?
          //       "token 1-3-2": {
          //        "$value": {
          //          "nested": {
          //            "nested again": {
          //              "$value": 123
          //            }
          //          }
          //        },
          $node = new DesignToken(
            $new_token_name,
            $group_or_token['$type'] ?? NULL,
            $group_or_token['$value'],
            $group_or_token['$description'] ?? ''
          );
          $root->addChild($node);
        } else {
          $group = new Group($new_token_name, $group_or_token['$description'] ?? '');
          // Is a group.
          $this->doParseDTCG($group_or_token, $new_token_name, $group);
          $root->addChild($group);
        }
      }
    }
  }

}
