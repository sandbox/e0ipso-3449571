<?php

declare(strict_types=1);

namespace Drupal\design_tokens\tom;

use Traversable;

class Group extends TomNode implements \IteratorAggregate
{
  /** @var TomNode[] */
  protected array $children;

  public function __construct(
    string $name,
    ?string $description = NULL,
  ) {
    parent::__construct($name, NULL, $description);
    $this->children = [];
  }

  public function addChild(TomNode $node): Group {
    if (!in_array($node, $this->children, true)) {
      $this->children[] = $node;
    }
    return $this;
  }

  public function removeChild(TomNode $node): Group {
    if (in_array($node, $this->children, true)) {
      $this->children = array_filter($this->children, fn($x) => $node !== $x);
    }
    return $this;
  }

  public function getIterator(): Traversable
  {
    return new \ArrayObject($this->children);
  }
}
