<?php

namespace Drupal\design_tokens\tom\Serializer;

use Drupal\design_tokens\tom\DesignToken;
use Drupal\design_tokens\tom\Group;
use Drupal\design_tokens\tom\RootGroup;
use Drupal\design_tokens\tom\TomNode;

class CssVarsSerializer
{

  public const GROUP_SEPARATOR = '.';
  public const CSS_SEPARATOR = '-';

  public function serialize(RootGroup $root): string {
    $vars = [];
    $this->doGenerateVars($root, $vars);
    return $this->generateCssFromVars($vars);
  }

  protected function doGenerateVars(TomNode $node, array &$vars): void {
    foreach ($node as $group_or_token) {
      if ($group_or_token instanceof Group) {
        $this->doGenerateVars($group_or_token, $vars);
      }
      elseif ($group_or_token instanceof DesignToken) {
        $vars[$group_or_token->name] = $group_or_token->value;
      }
    }
  }

  protected function generateCssFromVars(array $dtcgRefs): string
  {
    $styles = [];
    foreach ($dtcgRefs as $dtcgRefName => $dtcgRefOrValue) {
      $key = '--' . str_replace(static::GROUP_SEPARATOR, static::CSS_SEPARATOR, $dtcgRefName);
      if (is_string($dtcgRefOrValue) && str_starts_with($dtcgRefOrValue, '{') && str_ends_with($dtcgRefOrValue, '}')) {
        // Will be '{ref.eren.ce}', so we need to strip start and end,
        // and replace the group separator with css separator according to your
        // conventions.
        $refName = substr($dtcgRefOrValue, 1, -1);
        $cssVarName = str_replace(static::GROUP_SEPARATOR, static::CSS_SEPARATOR, $refName);
        $styles[$key] = sprintf('var(--%s)', $cssVarName);
      }
      else {
        // Plain value.
        if (!is_array($dtcgRefOrValue)) {
          $styles[$key] = $dtcgRefOrValue;
        }
        else {
          $styles[$key] = [];
          foreach ($dtcgRefOrValue as $propertyName => $propertyValue) {
            if (is_string($propertyValue) && str_starts_with($propertyValue, '{') && str_ends_with($propertyValue, '}')) {
              $propRefName = substr($propertyValue, 1, -1);
              $cssPropVarName = str_replace(static::GROUP_SEPARATOR, static::CSS_SEPARATOR, $propRefName);
              $styles[$key][$propertyName] = sprintf('var(--%s)', $cssPropVarName);
            }
            else {
              $styles[$key][$propertyName] = $propertyValue;
            }
          }
        }
      }
    }
    $css = ':root {' . PHP_EOL;
    foreach ($styles as $styleVarName => $styleVarValue) {
      if (is_array($styleVarValue)) {
        // @TODO if $type shadow, we need to format this properly:
        // box-shadow: none|h-offset v-offset blur spread color |inset|initial|inherit;
        $css .= "\t" . $styleVarName . ' {'. PHP_EOL;
        foreach ($styleVarValue as $propKey => $propValue) {
          if (!is_array($propValue)) {
            $css .= "\t\t" . $propKey . ': ' . $propValue . ';' .  PHP_EOL;
          }
          // @todo Handle nested properties? See test.tokens.json
          //       "token 1-3-2": {
          //        "$value": {
          //          "nested": {
          //            "nested again": {
          //              "$value": 123
          //            }
          //          }
          //        },
        }
        $css .= "\t}" . PHP_EOL;
      }
      else {
        $css .= "\t" . $styleVarName . ': ' . $styleVarValue . ';' . PHP_EOL;
      }
    }
    $css .= '}';
    return $css;
  }


}
