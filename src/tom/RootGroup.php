<?php

declare(strict_types=1);

namespace Drupal\design_tokens\tom;

class RootGroup extends Group
{

  protected const EMPTY_NAME = '';

  public function __construct(
  ) {
    parent::__construct(self::EMPTY_NAME);
  }

}
