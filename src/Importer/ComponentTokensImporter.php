<?php

namespace Drupal\design_tokens\Importer;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ComponentPluginManager;
use Drupal\Core\Utility\Error;
use Drupal\design_tokens\Entity\DesignToken;
use Psr\Log\LoggerInterface;

// @todo 2. This is not called anywhere. There should be a form, and a Drush command to reinspect all components.
// @todo 3. There should be a way to only consider one component, instead of all the components in the site.

/**
 * Importer class to create the design tokens defined in the components.
 */
final class ComponentTokensImporter {

  use StringTranslationTrait;

  /**
   * Creates a new ComponentTokensImporter object.
   *
   * @param \Drupal\Core\Theme\ComponentPluginManager $componentPluginManager
   *   The component plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(
    private readonly ComponentPluginManager $componentPluginManager,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly LoggerInterface $logger,
  ) {}

  /**
   * Imports the design tokens for the selected components.
   *
   * @param array $component_ids
   *   The IDs of the components to scan for tokens.
   *
   * @return int
   *   The number of created design tokens.
   */
  public function importTokens(array $component_ids = []): int {
    try {
      $storage = $this->entityTypeManager->getStorage('design_token');
    }
    catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      Error::logException($this->logger, $e);
      return 0;
    }
    $token_values = $this->buildComponentTokenValues($storage, $component_ids);
    return count(array_map(
      function(array $values) use ($storage) {
        try {
          return $storage->save($storage->create($values));
        }
        catch (EntityStorageException $e) {
          Error::logException($this->logger, $e);
          return 0;
        }
      },
      $token_values,
    ));
  }

  /**
   * Builds the token values to create the tokens.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The design token storage.
   * @param string[] $component_ids
   *   Restrict the scan to these component IDs. Pass an empty array to scan all
   *   the components in the site.
   *
   * @return \Drupal\design_tokens\Entity\DesignToken[]
   *   The imported entities.
   */
  private function buildComponentTokenValues(EntityStorageInterface $storage, array $component_ids): array {
    $component_definitions = $this->componentPluginManager->getDefinitions();
    if (!empty($component_ids)) {
      $component_definitions = array_intersect_key($component_definitions, array_flip($component_ids));
    }
    $defs_with_token_info = array_filter(
      $component_definitions,
      static fn(array $component_definition) => !empty($component_definition['thirdPartySettings']['design_tokens']['tokens'])
    );
    return array_reduce(
      $defs_with_token_info,
      function(array $token_values, array $component_definition) use ($storage): array {
        $token_data = $component_definition['thirdPartySettings']['design_tokens']['tokens'];
        foreach ($token_data as $component_token_name => $component_token_info) {
          $autocreate_token = $this->autocreateFallback($component_token_name, $component_token_info, $storage);
          if (!$autocreate_token) {
            continue;
          }
          $message = $this->validateComponentToken($component_definition, $component_token_name, $component_token_info['value'] ?? NULL);
          if ($message) {
            $this->logger->error($message);
            continue;
          }
          $component_tokens = $storage->loadByProperties(['name' => $component_token_name]);
          $component_token = reset($component_tokens);
          if ($component_token) {
            continue;
          }
          $token_values[] = [
            'id' => preg_replace('/\W/', '_', $component_token_name),
            'name' => $component_token_name,
            'value' => $autocreate_token->id(),
            'layer' => DesignToken::LAYER_COMPONENT,
          ];
        }
        return $token_values;
      },
      [],
    );
  }

  /**
   * Validates a component token before creation.
   *
   * @param array $component_definition
   *   The plugin definition for the component we are dealing with.
   * @param string $token_id
   *   The token ID the component defines.
   * @param mixed $token_value
   *   The token alias to connect the component token to.
   *
   * @return \Drupal\Component\Render\MarkupInterface|null
   *   The error message, or NULL if none.
   */
  private function validateComponentToken(array $component_definition, string $token_id, mixed $token_value): ?MarkupInterface {
    if (
      empty($token_value)
      || !is_string($token_value)
      || !str_starts_with($token_id, sprintf('component.%s.', $component_definition['machineName']))
    ) {
      return $this->t('Invalid component token. Make sure that your component declares tokens in the %s.component.yml. Put the token information under <code>thirdPartySettings.design_tokens.tokens</code>. The component token should start with <code>"component.%s."</code>, and it should have a <code>value:</code> key that is a non-empty string.');
    }
    return NULL;
  }

  /**
   * Validates the primitive token info before we can autocreate it.
   *
   * @param string $component_token_name
   *   The name for the component token.
   * @param array $token_input
   *   The token data from the component definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage for design tokens.
   *
   * @return \Drupal\Core\Config\Entity\ConfigEntityInterface|null
   *   The autocreated token or NULL.
   */
  private function autocreateFallback(
    string $component_token_name,
    array $token_input,
    EntityStorageInterface $storage
  ): ?ConfigEntityInterface {
    $autocreate_value = $token_input['fallback']['value'] ?? '';
    $autocreate_type = $token_input['fallback']['type'] ?? '';
    if (
      empty($autocreate_value)
      || !in_array($autocreate_type, DesignToken::getAllValidTypes(), TRUE)
    ) {
      $this->logger(
        $this->t(
          'The autocreate token "%token_id" needs a non-empty value, and a valid type. Valid token types are: %valid_types.',
          [
            '%token_id' => $autocreate_value,
            '%valid_types' => implode(', ', DesignToken::getAllValidTypes()),
          ]
        )
      );
      return NULL;
    }
    $autocreate_name = $token_input['value'] ?? NULL;
    $autocreate_tokens = $storage->loadByProperties(['name' => $autocreate_name]);
    $autocreate_token = reset($autocreate_tokens);
    if (!$autocreate_token) {
      $autocreate_token = $storage->create([
        'id' => preg_replace('/\W/', '_', $autocreate_name),
        'name' => $autocreate_name,
        'description' => '',
        'value' => $autocreate_value,
        'type' => $autocreate_type,
        'layer' => DesignToken::LAYER_PRIMITIVE,
        'needsSiteBuilderReview' => TRUE,
        'autoCreatedFrom' => $component_token_name,
      ]);
      assert($autocreate_token instanceof ConfigEntityInterface);
      try {
        $storage->save($autocreate_token);
      }
      catch (EntityStorageException $e) {
        $this->logger->error(
          $this->t(
            'There was an error deleting the temporary design token "%token_id". Error: %error',
            ['%token_id' => $autocreate_name, '%error' => $e->getMessage()]
          )
        );
        return NULL;
      }
      return $autocreate_token;
    }
    assert($autocreate_token instanceof ConfigEntityInterface);
    if (
      !$autocreate_token->get('needsSiteBuilderReview')
      || $autocreate_token->get('autoCreatedFrom') !== $component_token_name
    ) {
      return $autocreate_token;
    }
    // If the auto-created token was never validated, we update it.
    if ($autocreate_token->get('value') !== $autocreate_value) {
      try {
        // Update the auto-created token with the new values.
        $autocreate_token->set('value', $autocreate_value);
        $autocreate_token->set('type', $autocreate_type);
        $autocreate_token->set('layer', DesignToken::LAYER_PRIMITIVE);
        $autocreate_token->save();
      }
      catch (EntityStorageException $e) {
        $this->logger->error(
          $this->t(
            'There was an error updating the temporary design token "%token_id". Error: %error',
            ['%token_id' => $autocreate_name, '%error' => $e->getMessage()]
          )
        );
      }
    }
    return $autocreate_token;
  }

}
