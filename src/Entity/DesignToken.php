<?php

declare(strict_types=1);

namespace Drupal\design_tokens\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\design_tokens\DesignTokenInterface;

/**
 * Defines the design token entity type.
 *
 * @ConfigEntityType(
 *   id = "design_token",
 *   label = @Translation("Design Token"),
 *   label_collection = @Translation("Design Tokens"),
 *   label_singular = @Translation("design token"),
 *   label_plural = @Translation("design tokens"),
 *   label_count = @PluralTranslation(
 *     singular = "@count design token",
 *     plural = "@count design tokens",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\design_tokens\DesignTokenListBuilder",
 *     "form" = {
 *       "add" = "Drupal\design_tokens\Form\DesignTokenForm",
 *       "edit" = "Drupal\design_tokens\Form\DesignTokenForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "design_token",
 *   admin_permission = "administer design_token",
 *   links = {
 *     "collection" = "/admin/config/user-interface/design-token",
 *     "add-form" = "/admin/config/user-interface/design-token/add",
 *     "edit-form" =
 *   "/admin/config/user-interface/design-token/{design_token}",
 *     "delete-form" =
 *   "/admin/config/user-interface/design-token/{design_token}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "needsSiteBuilderReview",
 *     "autoCreatedFrom",
 *     "name",
 *     "type",
 *     "description",
 *     "value",
 *     "layer",
 *   },
 * )
 */
final class DesignToken extends ConfigEntityBase implements DesignTokenInterface {

  public const TYPE_COLOR = 'color';

  public const TYPE_DIMENSION = 'dimension';

  public const TYPE_FONT_FAMILY = 'fontFamily';

  public const TYPE_FONT_WEIGHT = 'fontWeight';

  public const TYPE_DURATION = 'duration';

  public const TYPE_CUBIC_BEIZER = 'cubicBeizer';

  public const TYPE_NUMBER = 'number';

  public const TYPE_STROKE = 'stroke';

  public const TYPE_BORDER = 'border';

  public const TYPE_TRANSITION = 'transition';

  public const TYPE_SHADOW = 'shadow';

  public const TYPE_GRADIENT = 'gradient';

  public const TYPE_TYPOGRAPHY = 'typography';

  protected const VALID_TYPES = [
    self::TYPE_COLOR,
    self::TYPE_DIMENSION,
    self::TYPE_FONT_FAMILY,
    self::TYPE_FONT_WEIGHT,
    self::TYPE_DURATION,
    self::TYPE_CUBIC_BEIZER,
    self::TYPE_NUMBER,
    self::TYPE_STROKE,
    self::TYPE_BORDER,
    self::TYPE_TRANSITION,
    self::TYPE_SHADOW,
    self::TYPE_GRADIENT,
    self::TYPE_TYPOGRAPHY,
  ];

  public const LAYER_PRIMITIVE = 'primitive';
  public const LAYER_SEMANTIC = 'semantic';
  public const LAYER_COMPONENT = 'component';

  /**
   * The example ID.
   */
  protected string $id;

  /**
   * The example label.
   */
  protected string $name;

  /**
   * The example description.
   */
  protected ?string $description;

  /**
   * The token type as defined in the DTCG.
   *
   * @see https://tr.designtokens.org/format/#design-token-properties
   */
  protected ?string $type;

  /**
   * The token value.
   *
   * It can be any JSON primitive.
   */
  protected mixed $value;

  /**
   * The token layer.
   *
   * @var string
   */
  protected string $layer = '';

  /**
   * Tracks weather a design token was auto-created and needs manual review.
   */
  protected bool $needsSiteBuilderReview = FALSE;

  /**
   * If the token was auto-created by association, the token ID for it.
   *
   * @var string
   */
  protected ?string $autoCreatedFrom;

  /**
   * Get the valid token types according to the DTCG.
   *
   * @return string[]
   */
  public static function getAllValidTypes(): array {
    return static::VALID_TYPES;
  }

  public function isPrimitive(): bool {
    return $this->layer === self::LAYER_PRIMITIVE;
  }

  /**
   * @return string
   */
  public function getType(): ?string
  {
    return $this->type;
  }

  /**
   * @return mixed
   */
  public function getValue(): mixed
  {
    return $this->value;
  }

  /**
   * @return string
   */
  public function getDescription(): ?string
  {
    return $this->description;
  }

  public function getReferencedToken(): ?DesignTokenInterface {
    if ($this->isPrimitive()) {
      return NULL;
    }
    $referenced = static::load($this->value);
    return $referenced instanceof DesignTokenInterface ? $referenced : NULL;
  }

}
