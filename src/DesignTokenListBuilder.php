<?php

declare(strict_types=1);

namespace Drupal\design_tokens;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\design_tokens\Entity\DesignToken;
use Drupal\design_tokens\tom\Serializer\CssVarsSerializer;
use Drupal\devel\Render\FilteredMarkup;

/**
 * Provides a listing of design tokens.
 */
final class DesignTokenListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['layer'] = $this->t('Layer');
    $header['label'] = $this->t('Label');
    $header['status'] = $this->t('Status');
    $header['type'] = $this->t('Type');
    $header['value'] = $this->t('Value');
    $header['auto-refresh'] = $this->t('Auto-refresh?');
    $header['css-var'] = $this->t('CSS Variable');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    assert($entity instanceof DesignTokenInterface);
    $row['layer'] = $entity->get('layer');
    $row['label'] = $entity->label();
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    $row['type'] = $entity->get('type');
    $value = $entity->isPrimitive()
      ? Yaml::encode($entity->get('value'))
      : '{' . $entity->getReferencedToken()?->label() . '}';
    $row['value'] = FilteredMarkup::create(sprintf('<code>%s</code>', $value));
    $autorefresh = $entity->get('needsSiteBuilderReview');
    $row['auto-refresh'] = '';
    if ($entity->get('layer') === DesignToken::LAYER_PRIMITIVE) {
      $row['auto-refresh'] = $autorefresh ? $this->t('Yes ℹ') : $this->t('No');
    }
    $row['css-var'] = $entity->get('layer') === DesignToken::LAYER_COMPONENT
      ? FilteredMarkup::create(sprintf('<code>--%s</code>', str_replace(CssVarsSerializer::GROUP_SEPARATOR, CssVarsSerializer::CSS_SEPARATOR, $entity->get('name'))))
      : '';
    if ($this->validateTokenReference($entity)) {
      $row['value'] = [
        'style' => 'background-color: rgba(255, 0, 0, 0.2)',
        'data' => $row['value'],
      ];
    }
    $full_row = $row + parent::buildRow($entity);
    if ($autorefresh) {
      $full_row = [
        'style' => 'background-color: rgba(220, 220, 50, 0.1)',
        'data' => $full_row,
      ];
    }
    return $full_row;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['description'] = [
      '#markup' => $this->t('ℹ This primitive token was auto-created from the fallback metadata in the parent component token. The token will auto-refresh if this token\'s metadata changes to a new fallback value in the <code>*.component.yml</code> file. To lock this token\'s value and prevent auto-refresh, manually save the token to confirm it.'),
    ];
    return $build;
  }

  /**
   * Validates the reference of the token.
   *
   * @param \Drupal\design_tokens\Entity\DesignToken $token
   *   The token to validate.
   *
   * @return int
   *   The number of errors.
   */
  private function validateTokenReference(DesignToken $token): int {
    $errors = match ($token->get('layer')) {
      DesignToken::LAYER_COMPONENT => $this->validateComponentTokenReference($token),
      DesignToken::LAYER_SEMANTIC => $this->validateSemanticTokenReference($token),
      DesignToken::LAYER_PRIMITIVE => $this->validatePrimitiveTokenReference($token),
    };
    return count(array_map(
      [$this->messenger(), 'addError'],
      $errors
    ));
  }

  /**
   * Validates the component token reference.
   *
   * @param \Drupal\design_tokens\Entity\DesignToken $token
   *   The token.
   *
   * @return array
   *   The errors.
   */
  private function validateComponentTokenReference(DesignToken $token): array {
    $errors = [];
    $referenced_token = DesignToken::load($token->getValue());
    if ($referenced_token && $referenced_token->isPrimitive()) {
      $errors[] = $this->t('Component tokens should not reference primitive tokens directly. Re-use, or create, a semantic token to reference in %token_name.', ['%token_name' => $token->label()]);
    }
    return $errors;
  }

  /**
   * Validates the semantic token reference.
   *
   * @param \Drupal\design_tokens\Entity\DesignToken $token
   *   The token.
   *
   * @return array
   *   The errors.
   */
  private function validateSemanticTokenReference(DesignToken $token): array {
    $errors = [];
    $referenced_token = DesignToken::load($token->getValue());
    if ($referenced_token && $referenced_token->get('layer') === DesignToken::LAYER_COMPONENT) {
      $errors[] = $this->t('Component tokens cannot be referenced. Make sure that %token_name references another semantic or primitive token.', ['%token_name' => $token->label()]);
    }
    return $errors;
  }

  /**
   * Validates the primitive token type.
   *
   * @param \Drupal\design_tokens\Entity\DesignToken $token
   *   The token.
   *
   * @return array
   *   The errors.
   */
  private function validatePrimitiveTokenReference(DesignToken $token): array {
    $errors = [];
    if (!in_array($token->getType(), DesignToken::getAllValidTypes(), TRUE)) {
      $errors[] = $this->t('Primitive tokens must have a type associated to them. Edit %token_name to give it a type.', ['%token_name' => $token->label()]);
    }
    return $errors;
  }

}
