<?php

declare(strict_types=1);

namespace Drupal\design_tokens;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a design token entity type.
 */
interface DesignTokenInterface extends ConfigEntityInterface
{

  public function isPrimitive(): bool;

  public function getType(): ?string;

  public function getDescription(): ?string;

  public function getValue(): mixed;

  public function getReferencedToken(): ?DesignTokenInterface;

}
