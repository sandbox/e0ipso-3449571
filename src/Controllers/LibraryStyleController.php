<?php

namespace Drupal\design_tokens\Controllers;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\design_tokens\ConfigTomParser;
use Drupal\design_tokens\tom\Group;
use Drupal\design_tokens\tom\Parser\DTCGParser;
use Drupal\design_tokens\tom\RootGroup;
use Drupal\design_tokens\tom\Serializer\CssVarsSerializer;
use Symfony\Component\HttpFoundation\Response;

class LibraryStyleController extends ControllerBase
{

  public function styles()
  {
    // Use the DTCG formatted design.tokens.json file.
//    $dtcg = Json::decode(file_get_contents(__DIR__ . '/../../design/design.tokens.json'));
//    $parser = new DTCGParser();
//    $tom = $parser->parse($dtcg);

    // Use stored Drupal config.
    $parser = new ConfigTomParser();
    $tom = $parser->parse();

    $serializer = new CssVarsSerializer();
    $styles = $serializer->serialize($tom);

    // @todo Resolve token aliases here or we can still defer it?
    // @todo Resolve sorting, the ones referencing should be after the referenced one or we don't care?
    return new Response($styles, 200, ['Content-Type' => 'text/css']);
  }

}
