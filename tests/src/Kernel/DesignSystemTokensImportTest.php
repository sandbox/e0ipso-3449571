<?php

namespace Drupal\Tests\design_tokens\Kernel;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\design_tokens\Entity\DesignToken;
use Drupal\design_tokens\Importer\ComponentTokensImporter;
use Drupal\KernelTests\KernelTestBase;

class DesignTokensImportTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'design_tokens', 'design_tokens_test'];

  /**
   * {@inheritdoc}
   */
  protected static $themes = ['sdc_theme_test'];

  /**
   * The design token importer.
   *
   * @var \Drupal\design_tokens\Importer\ComponentTokensImporter
   */
  protected ComponentTokensImporter $importer;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->importer = \Drupal::service(ComponentTokensImporter::class);
  }

  /**
   * Test that tokens are imported correctly.
   */
  public function testImportDesignTokens(): void {
    $storage = $this->entityTypeManager->getStorage('design_token');
    $this->assertFalse($storage->hasData());
    $this->importer->importTokens();
    $component_token = $storage->load('component_with_design_tokens_heading_lv2_font');
    $this->assertInstanceOf(DesignToken::class, $component_token);
    $fallback_token = $storage->load('drupal_branding_color_impact');
    $this->assertTrue($fallback_token->get('needsSiteBuilderReview'));
    $this->assertEquals('component.with-design-tokens.heading.color', $fallback_token->get('autoCreatedFrom'));
  }

}
